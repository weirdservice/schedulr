#!/usr/bin/env node
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _nodeRedisPubsub = require('node-redis-pubsub');

var _nodeRedisPubsub2 = _interopRequireDefault(_nodeRedisPubsub);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _child_process = require('child_process');

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

_commander2['default'].version(_packageJson2['default'].version).usage('<configurations..>').parse(process.argv);

var config = { port: 6379, scope: 'test' },
    nrp = new _nodeRedisPubsub2['default'](config);

var key = _crypto2['default'].randomBytes(32).toString('hex');

// db
_mongoose2['default'].connect('mongodb://localhost/test');
var db = _mongoose2['default'].connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  // do some pubsub
  console.log('DB Connected');
});

var siteSchema = _mongoose2['default'].Schema({
  site: String,
  feedUrl: String,
  created: { type: Date, 'default': Date.now },
  updated: { type: Date, 'default': Date.now },
  lastScrapped: { type: Date, 'default': Date.now }
});

var Sites = _mongoose2['default'].model('Sites', siteSchema);

var mock = {};
var requested = [];

var find = function find() {
  Sites.find({
    lastScrapped: { "$lt": Date.now() - 1000 * 60 }
  }, function (err, sites) {
    if (err) return console.error(err);

    if (sites.length > 0) {
      sites.forEach(function (site) {
        nrp.emit('scrapr:isUpdated', { url: site.feedUrl });
        nrp.emit('schedulr:scrapped', { id: site._id });
      });
    }
  });
};

var scrapped = function scrapped(data) {
  Sites.findById(data.id, function (err, site) {
    if (err) console.log(err);

    site.lastScrapped = Date.now();
    site.save(function (err) {
      if (err) return handleError(err);
    });
  });
};

var register = function register() {
  nrp.emit('castr:add:schedulr', { id: key });
};

nrp.on('schedulr:find:' + key, find);
nrp.on('schedulr:scrapped:' + key, scrapped);

nrp.on('castr:callback', register);

process.on('SIGINT', function () {
  console.log("\nBye!");
  nrp.emit('castr:remove:schedulr', { id: key });
  nrp.quit();
  process.exit();
});

register();
console.log('Listen schedulr:* as ' + key);

setInterval(function () {
  nrp.emit('schedulr:find', { called: Date.now() });
}, 1000);
