#!/usr/bin/env node
import program from 'commander'
import NRP from 'node-redis-pubsub'
import crypto from 'crypto'
import mongoose from 'mongoose'
import {spawn} from 'child_process'

import pkg from '../package.json'

program
  .version(pkg.version)
  .usage('<configurations..>')
  .parse(process.argv)

const config = { port: 6379, scope: 'test' },
      nrp = new NRP(config)

const key = crypto.randomBytes(32).toString('hex')

// db
mongoose.connect('mongodb://localhost/test')
var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function (callback) {
  // do some pubsub
  console.log('DB Connected')
})

const siteSchema = mongoose.Schema({
    site: String,
    feedUrl: String,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    lastScrapped: { type: Date, default: Date.now }
})

const Sites = mongoose.model('Sites', siteSchema)

var mock = {}
var requested = []

const find = () => {
  Sites.find({
      lastScrapped: { "$lt": Date.now() - (1000 * 60) }
    },
    (err, sites) => {
    if (err)
      return console.error(err)

    if (sites.length > 0) {
      sites.forEach((site) => {
        nrp.emit('scrapr:isUpdated', {url: site.feedUrl})
        nrp.emit('schedulr:scrapped', {id: site._id})
      })
    }
  })
}

const scrapped = (data) => {
  Sites.findById(data.id, (err, site) => {
    if(err) console.log(err)

    site.lastScrapped = Date.now()
    site.save((err) => {
      if (err) return handleError(err)
    })
  })
}

const register = () => {
  nrp.emit('castr:add:schedulr', {id: key})
}

nrp.on('schedulr:find:' + key, find)
nrp.on('schedulr:scrapped:' + key, scrapped)

nrp.on('castr:callback', register)

process.on('SIGINT', () => {
    console.log("\nBye!")
    nrp.emit('castr:remove:schedulr', {id: key})
    nrp.quit()
    process.exit()
})

register()
console.log('Listen schedulr:* as ' + key)

setInterval(() => {
  nrp.emit('schedulr:find', {called: Date.now()})
}, 1000)
